import { Injectable } from '@nestjs/common';
import { ConfigService as NestConfigService } from '@nestjs/config';

@Injectable()
export class ConfigService {
  constructor(private readonly configService: NestConfigService) {}

  isDevelopment(): boolean {
    return this.configService.get<string>('app.env') === 'development';
  }

  isProduction(): boolean {
    return this.configService.get<string>('app.env') === 'production';
  }

  isDebug(): boolean {
    return this.configService.get<boolean>('app.debug');
  }
}
