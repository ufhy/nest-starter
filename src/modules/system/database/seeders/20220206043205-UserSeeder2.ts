import { QueryInterface } from 'sequelize/types';
import { MigrationParams } from 'umzug';
import { v4 as uuidv4 } from 'uuid';
import { faker } from '@faker-js/faker';
import * as bcrypt from 'bcrypt';
import { format } from 'date-fns';

module.exports = {
  async up({ context: queryInterface }: MigrationParams<QueryInterface>) {
    const password = 'random_password';
    const saltOrRounds = 10;

    const data = [];
    for (let index = 0; index < 1000; index++) {
      const salt = await bcrypt.genSalt(saltOrRounds);
      const hash = await bcrypt.hash(password, salt);

      data.push({
        uid: uuidv4(),
        username: faker.internet.userName(),
        email: faker.internet.email(),
        password: hash,
        password_salt: salt,
        created_at: format(new Date(), 'yyyy-MM-dd HH:mm:ss'),
        updated_at: format(new Date(), 'yyyy-MM-dd HH:mm:ss'),
      });
    }

    await queryInterface.bulkInsert('sys_users', data);
  },

  async down({ context: queryInterface }: MigrationParams<QueryInterface>) {
    await queryInterface.bulkDelete('sys_users', null);
  },
};
