import { Command, CommandRunner, Option } from 'nest-commander';
import { MigrationMeta } from 'umzug';
import { CommandService } from '../command.service';

@Command({
  name: 'db:migrate',
  description: 'Run database migrations',
})
export class DbMigrate implements CommandRunner {
  constructor(private readonly commandService: CommandService) {}

  async run(passedParam: string[], options?: { force?: boolean }): Promise<void> {
    const umzug = this.commandService.umzug('migration');

    if (options.force === true) {
      const executed = await umzug.executed();
      for (let index = 0; index < executed.length; index++) {
        const element = executed[index];
        console.log('element', element);
        await umzug.down({ to: element.name });
      }
    }

    await umzug.up().then((payload: MigrationMeta[]) => {
      console.log();
      console.log(`${payload.length} migrations ran`, 'db:migrate');
    });
  }

  @Option({
    flags: '--force',
    description: 'Force the migration to run',
  })
  foreceMigration() {
    return true;
  }
}
