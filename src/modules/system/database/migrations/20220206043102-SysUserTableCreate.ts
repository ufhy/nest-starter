import { DataType } from 'sequelize-typescript';
import { QueryInterface } from 'sequelize/types';
import { MigrationParams } from 'umzug';

module.exports = {
  async up({ context: queryInterface }: MigrationParams<QueryInterface>) {
    await queryInterface.createTable('sys_users', {
      id: {
        type: DataType.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      uid: {
        type: DataType.UUID,
        defaultValue: DataType.UUIDV4,
        allowNull: false,
        unique: true,
      },
      username: {
        type: DataType.STRING,
        allowNull: false,
        unique: true,
      },
      email: {
        type: DataType.STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataType.STRING,
        allowNull: false,
      },
      password_salt: {
        type: DataType.STRING,
        allowNull: false,
      },
      remember_token: DataType.STRING(100),
      ip_address: DataType.STRING(45),
      last_login: DataType.DATE,
      last_seen: DataType.DATE,
      status: {
        type: DataType.BOOLEAN,
        defaultValue: true,
      },
      created_at: DataType.DATE,
      updated_at: DataType.DATE,
    });
  },

  async down({ context: queryInterface }) {
    await queryInterface.dropTable('sys_users');
  },
};
