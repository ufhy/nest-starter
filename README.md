<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

# Nest StarterKit

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository with [Sequelize]((https://sequelize.org/)) ORM.

## Installation

```bash
$ npm install
```
dont forget to rename `.env.example` to `.env`

## Commander

```bash
# Command list
$ npm run ufhy

# make migration file
$ npm run ufhy make:migration UserTableCreate

# make migration file at module folder
$ npm run ufhy make:migration UserTableCreate --module=system

# make seeder file
$ npm run ufhy make:seeder UserSeeder

# make seeder file at module folder
$ npm run ufhy make:seeder UserSeeder --module=system

# running migration
$ npm run ufhy db:migrate

# running migration with force
$ npm run ufhy db:migrate --force

# rollback migration
$ npm run ufhy db:migrate:rollback

# rollback all migration
$ npm run ufhy db:migrate:rollback --all

# running seeders
$ npm run ufhy db:seed

# running seeders with force
$ npm run ufhy db:seed --force

# rollback seeder
$ npm run ufhy db:seed:rollback

# rollback all seeder
$ npm run ufhy db:seed:rollback --all
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
