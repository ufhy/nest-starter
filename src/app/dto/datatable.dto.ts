import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export class DatatableQueryArgs {
  offset: number;
  limit: number;
  order: Array<{ column: string; desc: true }>;
}

export const DatatableQuery = createParamDecorator((data: unknown, context: ExecutionContext) => {
  const request = context.switchToHttp().getRequest();
  const result = new DatatableQueryArgs();
  result.offset = request.query.offset ? Number(request.query.offset) : 0;
  result.limit = request.query.limit ? Number(request.query.limit) : 25;

  result.order = [];
  if (request.query.order && Array.isArray(request.query.order)) {
    const order = [];
    request.query.order.forEach((o) => {
      if (o.column && o.desc) {
        order.push({ column: o.column, desc: o.desc === 'true' });
      }
    });
    console.log(order);
    result.order = order;
  }

  return result;
});
