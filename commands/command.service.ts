import { Injectable } from '@nestjs/common';
import { green, yellow } from 'colors';
import { Sequelize } from 'sequelize-typescript';
import { SequelizeStorage, Umzug } from 'umzug';
import { join } from 'path';
import { existsSync, mkdirSync, readFileSync, writeFileSync } from 'fs';
import { snakeCase } from 'snake-case';
import { pascalCase } from 'pascal-case';
import camelCase from 'camelcase';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const pluralize = require('pluralize');

function format(i: any) {
  return parseInt(i, 10) < 10 ? '0' + i : i;
}

@Injectable()
export class CommandService {
  constructor(private readonly sequelize: Sequelize) {}

  umzug(purpose: 'migration' | 'seeder') {
    return new Umzug({
      migrations: {
        glob: purpose === 'seeder' ? '{src/database/seeders/*.ts,src/modules/**/seeders/*.ts}' : '{src/database/migrations/*.ts,src/modules/**/migrations/*.ts}',
      },
      context: this.sequelize.getQueryInterface(),
      storage: new SequelizeStorage({
        sequelize: this.sequelize,
        tableName: purpose === 'seeder' ? 'seeders' : 'migrations',
        timestamps: true,
      }),
      logger: {
        info: (message: Record<string, unknown>) => {
          const event = message.event + ': ';
          if (message.event === 'migrating') {
            if (purpose === 'seeder') {
              console.log(yellow('seeding'), message.name);
            } else {
              console.log(yellow(event), message.name);
            }
          } else if (message.event === 'migrated') {
            if (purpose === 'seeder') {
              console.log(green('successfully'), message.name, message.durationSeconds);
            } else {
              console.log(green(event), message.name, message.durationSeconds);
            }
          } else if (message.event === 'reverting') {
            console.log(yellow(event), message.name);
          } else if (message.event === 'reverted') {
            console.log(green(event), message.name, message.durationSeconds);
          }
        },
        warn: (message: Record<string, unknown>) => {
          console.log(message);
        },
        error: (message: Record<string, unknown>) => {
          console.log(message);
        },
        debug: (message: Record<string, unknown>) => {
          console.log(message);
        },
      },
    });
  }

  generateUnixFileName(name: string, extension = '.ts') {
    const date = new Date();
    const prefix = [
      date.getUTCFullYear(),
      format(date.getUTCMonth() + 1),
      format(date.getUTCDate()),
      format(date.getUTCHours()),
      format(date.getUTCMinutes()),
      format(date.getUTCSeconds()),
    ].join('');

    return prefix + '-' + name + extension;
  }

  folderPath(purpose: 'migration' | 'seeder' | 'model', module?: string) {
    let folderPath = '';

    if (purpose === 'model') {
      folderPath = join(process.cwd(), 'src/models');
      if (module) {
        folderPath = join(process.cwd(), 'src/modules', module, 'models');
      }
    } else {
      folderPath = join(process.cwd(), 'src/database', purpose === 'seeder' ? 'seeders' : 'migrations');
      if (module) {
        folderPath = join(process.cwd(), 'src/modules', module, 'database', purpose === 'seeder' ? 'seeders' : 'migrations');
      }
    }

    if (!existsSync(folderPath)) {
      mkdirSync(folderPath, { recursive: true });
    }

    return folderPath;
  }

  async makeMigration(name: string, module?: string, table?: string) {
    let stubPath = join(__dirname, 'make/stubs/migration.stub');
    if (name.toLocaleLowerCase().includes('create')) {
      stubPath = join(__dirname, 'make/stubs/migration.create.stub');
    }

    const stub = await readFileSync(stubPath, 'utf8');
    const tableName = table ? pluralize.plural(snakeCase(table)) : pluralize.plural(snakeCase(name));
    const stubReplace = stub.replace(/\{{ table }}/gi, tableName);

    const folderPath = this.folderPath('migration', module);
    const fileName = folderPath + '/' + this.generateUnixFileName(pascalCase(name));

    await writeFileSync(fileName, stubReplace);

    return fileName;
  }

  async makeModel(name: string, module?: string, table?: string, migration?: boolean) {
    const stubPath = join(__dirname, 'make/stubs/model.stub');
    const stub = await readFileSync(stubPath, 'utf8');

    let stubReplace = stub.replace(/\{{ className }}/gi, pascalCase(name));

    const tableName = table ? pluralize.plural(snakeCase(table)) : pluralize.plural(snakeCase(name));
    stubReplace = stubReplace.replace(/\{{ table }}/gi, tableName);

    const folderPath = this.folderPath('model', module);
    const fileModel = folderPath + '/' + camelCase(name) + '.model.ts';
    if (existsSync(fileModel)) {
      throw new Error('Model already exists');
    }

    await writeFileSync(fileModel, stubReplace);

    if (migration) {
      const fileMigration = await this.makeMigration(name + 'TableCreate', module, tableName);

      return [fileModel, fileMigration];
    }

    return fileModel;
  }
}
