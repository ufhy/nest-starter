import { green, red } from 'colors';
import { Command, CommandRunner, Option } from 'nest-commander';
import { CommandService } from '../command.service';

@Command({
  name: 'make:model',
  arguments: '<name>',
  description: 'Create a new model file',
})
export class MakeModel implements CommandRunner {
  constructor(private readonly commandService: CommandService) {}

  async run(passedParam: string[], options?: { module?: string; table?: string; migration?: boolean }): Promise<void> {
    try {
      const makeModel = await this.commandService.makeModel(passedParam[0], options.module, options.table, options.migration);

      console.log(green('Created model: '), makeModel);
    } catch (error) {
      console.log(red(error));
    }
  }

  @Option({
    flags: '--module <module>',
    description: 'Create a new seeder file for a module (e.g. --module=users)',
  })
  parseModuleName(value: string) {
    return value;
  }

  @Option({
    flags: '-t, --table <table>',
    description: 'Create a new seeder file for a table (e.g. --table=users)',
  })
  parseTableName(value: string) {
    return value;
  }

  @Option({
    flags: '-mg, --migration',
    description: 'Create a new migration file',
  })
  parseMigration(value: boolean) {
    return value;
  }
}
