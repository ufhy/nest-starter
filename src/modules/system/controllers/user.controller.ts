import { Controller, Get } from '@nestjs/common';
import { DatatableQuery, DatatableQueryArgs } from '../../../app/dto/datatable.dto';
import { UserService } from '../services/user.service';

@Controller('system/users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  findAll(@DatatableQuery() query: DatatableQueryArgs) {
    return this.userService.datatable(query);
  }
}
