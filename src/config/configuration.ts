import { registerAs } from '@nestjs/config';

export const app = registerAs('app', () => ({
  name: process.env.APP_NAME,
  port: parseInt(process.env.APP_PORT),
  env: process.env.APP_ENV,
  debug: process.env.APP_DEBUG === 'true',
}));

export const database = registerAs('database', () => ({
  host: process.env.DB_HOST,
  port: parseInt(process.env.DB_PORT),
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  dialect: process.env.DB_DIALECT,
}));
