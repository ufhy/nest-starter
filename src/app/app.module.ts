import { Module } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';
import { DatabaseModule } from '../database/database.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import modules from '../modules';

@Module({
  imports: [ConfigModule, DatabaseModule, ...modules],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
