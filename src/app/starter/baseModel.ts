import { Model } from 'sequelize-typescript';

export class BaseModel extends Model {
  protected exclude: string[] = [];

  toJSON() {
    const attributes = Object.assign({}, this.get());
    for (const a of this.exclude) {
      delete attributes[a];
    }
    return attributes;
  }
}
