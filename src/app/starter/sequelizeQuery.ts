import { ModelCtor } from 'sequelize-typescript';
import { FindOptions, WhereOptions, Op } from 'sequelize';

type TInclude = {
  model?: ModelCtor;
  as?: string;
  separate?: boolean; // separated query
  include?: Array<ModelCtor | TInclude>;
};

type TWithCallback = () => typeof SequelizeQueryOption;

export class SequelizeQueryOption {
  private _wheres: WhereOptions<any> = {};

  private _includes: Array<ModelCtor | TInclude> = [];

  private _limit?: number;

  private _offset?: number;

  /**
   * Example: where('somefield', Op.eq, 'somevalue')
   * will generated:
   * {
   *      somefield: {
   *          [Op.eq]: 'somevalue'
   *      }
   * }
   *
   * Example: where('somefield', 'somevalue')
   * will generated:
   * {
   *      somefield: 'somevalue'
   * }
   */
  public where(column: string, operator: any, value?: any) {
    if (value) {
      this._wheres[column] = {
        [operator]: value,
      };
    } else {
      this._wheres[column] = operator;
    }

    return this;
  }

  /**
         * Example: orWhere([
                    { column: 'abbr', operator: Op.ne, value: 'HK' },
                    { column: 'abbr', operator: 'BJU' },
                ])
         */
  public orWhere(payloads: Array<{ column: string; operator: any; value?: any }>) {
    const wheres: Array<any> = [];
    payloads.forEach((payload) => {
      if (payload.value) {
        wheres.push({
          [payload.column]: {
            [payload.operator]: payload.value,
          },
        });
      } else {
        wheres.push({ [payload.column]: payload.operator });
      }
    });

    this._wheres[Op.or] = wheres;

    return this;
  }

  /**
   * Query: LIMIT
   */
  public take(value: number) {
    this._limit = value;

    return this;
  }

  /**
   * OFFSET
   */
  public skip(value: number) {
    this._offset = value;

    return this;
  }

  // private parseWithCallback(callback: TWithCallback): TInclude {
  //   const calling = callback();
  //   return calling.generated();
  // }

  /**
   * Contoh umum:
   * with(SomeModel)
   *
   * Contoh memiliki relasi lebih dari 1 dengan model yang sama
   * with({mode: SomeModel, as: 'SomeModelAlias'})
   */
  // public with(relate: ModelCtor | TInclude, nestedCallback?: TWithCallback) {
  //   if (nestedCallback) {
  //     const parsed = nestedCallback().generated();
  //     console.log(parsed);
  //     if (typeof relate === 'function') {
  //       this._includes.push({
  //         model: relate,
  //         include: parsed['include'],
  //       });
  //     }
  //     if (typeof relate === 'object') {
  //       this._includes.push({
  //         model: relate.model,
  //         include: parsed['include'],
  //       });
  //     }
  //   } else {
  //     this._includes.push(relate);
  //   }

  //   return this;
  // }

  public generated(): Record<any, any> {
    const result: FindOptions = {
      where: this._wheres,
      limit: this._limit,
      offset: this._offset,
      include: this._includes,
    };

    return result;
  }
}
