import { Column, DataType, PrimaryKey, Table } from 'sequelize-typescript';
import { BaseModel } from '@/app/starter/baseModel';

@Table({
  tableName: 'sys_users',
  underscored: true,
})
export class SysUser extends BaseModel {
  @PrimaryKey
  @Column({ autoIncrement: true })
  id: number;

  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    allowNull: false,
    unique: true,
  })
  uid: string;

  @Column({ unique: true, allowNull: false })
  username: string;

  @Column({ unique: true, allowNull: false })
  email: string;

  @Column({ allowNull: false })
  password: string;

  @Column({ allowNull: false })
  password_salt: string;

  @Column
  remember_token?: string;

  @Column
  ip_address?: string;

  @Column({ type: DataType.DATE })
  last_login?: string;

  @Column({ type: DataType.DATE })
  last_seen?: string;

  @Column({ type: DataType.BOOLEAN })
  status: boolean;
}
