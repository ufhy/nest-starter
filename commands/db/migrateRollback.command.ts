import { Command, CommandRunner, Option } from 'nest-commander';
import { CommandService } from '../command.service';

@Command({
  name: 'db:migrate:rollback',
  description: 'Rollback database migrations',
})
export class DbMigrateRollback implements CommandRunner {
  constructor(private readonly commandService: CommandService) {}

  async run(passedParam: string[], options?: { all?: boolean }): Promise<void> {
    const umzug = this.commandService.umzug('migration');

    if (options.all === true) {
      const executed = await umzug.executed();
      for (let index = 0; index < executed.length; index++) {
        const element = executed[index];
        console.log('element', element);
        await umzug.down({ to: element.name });
      }
    } else {
      await umzug.down();
    }
  }

  @Option({
    flags: '--all',
    description: 'Rollback all database migrations',
  })
  foreceMigration() {
    return true;
  }
}
