import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';
import { Dialect } from 'sequelize/dist';

@Module({
  imports: [
    SequelizeModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          dialect: configService.get<Dialect>('database.dialect'),
          host: configService.get<string>('database.host'),
          port: configService.get<number>('database.port'),
          username: configService.get<string>('database.username'),
          password: configService.get<string>('database.password'),
          database: configService.get<string>('database.database'),
          synchronize: false,
          autoLoadModels: true,
          logQueryParameters: configService.get<boolean>('app.debug'),
          benchmark: configService.get<boolean>('app.debug'),
          logging: configService.get<boolean>('app.debug') ? console.log : false,
        };
      },
    }),
  ],
})
export class DatabaseModule {}
