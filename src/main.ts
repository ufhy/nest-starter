import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get(ConfigService);

  const port = config.get<number>('APP_PORT');
  const environment = config.get<string>('APP_ENV');
  const debug = config.get<boolean>('APP_DEBUG');

  app.useLogger(debug ? new Logger() : false);
  await app.listen(port, () => {
    console.log();
    console.log('============================================================');
    console.log(`Server running on http://localhost:${port}`);
    console.log(`Environment: ${environment}`);
    console.log(`Debug: ${debug}`);
    console.log('============================================================');
  });
}
bootstrap();
