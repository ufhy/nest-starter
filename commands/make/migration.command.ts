import { green, red } from 'colors';
import { readFileSync, writeFileSync } from 'fs';
import { Command, CommandRunner, Option } from 'nest-commander';
import { join } from 'path';
import { CommandService } from '../command.service';

@Command({
  name: 'make:migration',
  arguments: '<name>',
  description: 'Create a new migration file',
})
export class MakeMigration implements CommandRunner {
  constructor(private readonly commandService: CommandService) {}

  async run(passedParam: string[], options?: { module?: string; table?: string }): Promise<void> {
    try {
      const makeMigration = await this.commandService.makeMigration(passedParam[0], options.module, options.table);
      console.log(green('Created migration: '), makeMigration);
    } catch (error) {
      console.log(red(error));
    }
  }

  @Option({
    flags: '-m, --module <module>',
    description: 'Create a new migration file for a module (e.g. --module=users)',
  })
  parseModuleName(value: string) {
    return value;
  }

  @Option({
    flags: '-t, --table <table>',
    description: 'Create a new migration file for a table (e.g. --table=users)',
  })
  parseTableName(value: string) {
    return value;
  }
}
