import { Command, CommandRunner, Option } from 'nest-commander';
import { MigrationMeta } from 'umzug';
import { CommandService } from '../command.service';

@Command({
  name: 'db:seed',
  description: 'Run database seeders',
})
export class DbSeed implements CommandRunner {
  constructor(private readonly commandService: CommandService) {}

  async run(passedParam: string[], options?: { force?: boolean }): Promise<void> {
    const umzug = this.commandService.umzug('seeder');

    if (options.force === true) {
      const executed = await umzug.executed();
      for (let index = 0; index < executed.length; index++) {
        const element = executed[index];
        console.log('element', element);
        await umzug.down({ to: element.name });
      }
    }

    await umzug.up().then((payload: MigrationMeta[]) => {
      console.log();
      console.log(`${payload.length} seeders ran`, 'db:seed');
    });
  }

  @Option({
    flags: '--force',
    description: 'Force the seeders to run',
  })
  foreceMigration() {
    return true;
  }
}
