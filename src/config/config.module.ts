import { Module } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from '@nestjs/config';
import { ConfigService } from './config.service';
import { app, database } from './configuration';
import validationSchema from './validationSchema';

@Module({
  imports: [
    NestConfigModule.forRoot({
      cache: true,
      validationSchema: validationSchema,
      load: [app, database],
    }),
  ],
  providers: [ConfigService],
})
export class ConfigModule {}
