import { Command, CommandRunner, Option } from 'nest-commander';
import { CommandService } from '../command.service';

@Command({
  name: 'db:seed:rollback',
  description: 'Rollback database seeders',
})
export class DbSeedRollback implements CommandRunner {
  constructor(private readonly commandService: CommandService) {}

  async run(passedParam: string[], options?: { all?: boolean }): Promise<void> {
    const umzug = this.commandService.umzug('seeder');

    if (options.all === true) {
      const executed = await umzug.executed();
      for (let index = 0; index < executed.length; index++) {
        const element = executed[index];
        console.log('element', element);
        await umzug.down({ to: element.name });
      }
    } else {
      await umzug.down();
    }
  }

  @Option({
    flags: '--all',
    description: 'Rollback all database seeders',
  })
  foreceMigration() {
    return true;
  }
}
