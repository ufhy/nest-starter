import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { DatatableQueryArgs } from '../../../app/dto/datatable.dto';
import { SysUser } from '../models/sysUser.model';

// columns[0][data]: uid
// columns[0][name]:
// columns[0][searchable]: false
// columns[0][orderable]: false
// columns[0][search][value]:
// columns[0][search][regex]: false
// columns[1][data]: code
// columns[1][name]:
// columns[1][searchable]: true
// columns[1][orderable]: true
// columns[1][search][value]:
// columns[1][search][regex]: false
// columns[2][data]: name
// columns[2][name]:
// columns[2][searchable]: true
// columns[2][orderable]: true
// columns[2][search][value]:
// columns[2][search][regex]: false
// columns[3][data]: abbr
// columns[3][name]:
// columns[3][searchable]: true
// columns[3][orderable]: true
// columns[3][search][value]:
// columns[3][search][regex]: false
// columns[4][data]: website
// columns[4][name]:
// columns[4][searchable]: true
// columns[4][orderable]: true
// columns[4][search][value]:
// columns[4][search][regex]: false
// columns[5][data]: status
// columns[5][name]:
// columns[5][searchable]: false
// columns[5][orderable]: true
// columns[5][search][value]:
// columns[5][search][regex]: false
// order[0][column]: 2
// order[0][dir]: asc
// start: 0
// length: 25
// search[regex]: false

@Injectable()
export class UserService {
  constructor(@InjectModel(SysUser) private readonly sysUser: typeof SysUser) {}

  datatable(payload: DatatableQueryArgs) {
    const order = [];
    if (payload.order && payload.order.length > 0) {
      payload.order.forEach((o) => {
        order.push([o.column, o.desc ? 'DESC' : 'ASC']);
      });
    }

    return this.sysUser.findAll({
      offset: payload.offset,
      limit: payload.limit,
      order: order,
    });
  }
}
