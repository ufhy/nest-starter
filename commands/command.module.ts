import { Module } from '@nestjs/common';
import { ConfigModule } from '../src/config/config.module';
import { DatabaseModule } from '../src/database/database.module';
import { CommandService } from './command.service';
import { DbMigrate } from './db/migrate.command';
import { DbMigrateRollback } from './db/migrateRollback.command';
import { DbSeed } from './db/seed.command';
import { DbSeedRollback } from './db/seedRollback.command';
import { MakeMigration } from './make/migration.command';
import { MakeModel } from './make/model.command';
import { MakeSeeder } from './make/seeder.command';

@Module({
  imports: [ConfigModule, DatabaseModule],
  providers: [CommandService, MakeMigration, MakeSeeder, MakeModel, DbMigrate, DbMigrateRollback, DbSeed, DbSeedRollback],
})
export class CommandModule {}
