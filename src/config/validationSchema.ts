import * as Joi from 'joi';

export default Joi.object({
  APP_NAME: Joi.string().required(),
  APP_PORT: Joi.number().default(3000),
  APP_ENV: Joi.string().required().valid('development', 'production').default('development'),
  APP_DEBUG: Joi.boolean().default(true),

  DB_DIALECT: Joi.required().valid('mysql', 'postgres').default('mysql'),
  DB_HOST: Joi.required().default('localhost'),
  DB_PORT: Joi.required().default(3306),
  DB_USERNAME: Joi.required(),
  DB_PASSWORD: Joi.required(),
  DB_DATABASE: Joi.required(),
});
