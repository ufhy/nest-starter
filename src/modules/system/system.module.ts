import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { UserController } from './controllers/user.controller';
import { SysUser } from './models/sysUser.model';
import { UserService } from './services/user.service';

@Module({
  imports: [SequelizeModule.forFeature([SysUser])],
  providers: [UserService],
  controllers: [UserController],
})
export class SystemModule {}
