import { green, red } from 'colors';
import { readFileSync, writeFileSync } from 'fs';
import { Command, CommandRunner, Option } from 'nest-commander';
import { join } from 'path';
import { snakeCase } from 'snake-case';
import { CommandService } from '../command.service';

@Command({
  name: 'make:seeder',
  arguments: '<name>',
  description: 'Create a new seeder file',
})
export class MakeSeeder implements CommandRunner {
  constructor(private readonly commandService: CommandService) {}

  async run(passedParam: string[], options?: { module?: string; table?: string }): Promise<void> {
    try {
      const stubPath = join(__dirname, 'stubs/seeder.stub');
      const stub = await readFileSync(stubPath, 'utf8');
      const tableName = options.table ? snakeCase(options.table) : '';
      const stubReplace = stub.replace(/\{{ table }}/gi, tableName);

      const folderPath = this.commandService.folderPath('seeder', options.module);
      const fileName = folderPath + '/' + this.commandService.generateUnixFileName(passedParam[0]);
      await writeFileSync(fileName, stubReplace);

      console.log(green('Created seeder: '), fileName);
    } catch (error) {
      console.log(red(error));
    }
  }

  @Option({
    flags: '-m, --module <module>',
    description: 'Create a new seeder file for a module (e.g. --module=users)',
  })
  parseModuleName(value: string) {
    return value;
  }

  @Option({
    flags: '-t, --table <table>',
    description: 'Create a new seeder file for a table (e.g. --table=users)',
  })
  parseTableName(value: string) {
    return value;
  }
}
